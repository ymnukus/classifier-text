package api

import (
	v1 "classify/routes/api/v1"

	"github.com/labstack/echo/v4"
)

func Init(e *echo.Group) {
	v1.Init(e.Group("/v1"))
}
