package v1

import (
	"classify/libs"
	"classify/structs"
	"encoding/json"
	"net/http"

	"github.com/labstack/echo/v4"
)

func Init(e *echo.Group) {
	e.POST("/naivebayes", naivebayes)
	e.POST("/fisher", fisher)
}

func naivebayes(c echo.Context) error {
	var err error
	var text struct {
		Text string `json:"text"`
	}
	err = json.NewDecoder(c.Request().Body).Decode(&text)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, structs.Result{
			Result:  false,
			Code:    http.StatusInternalServerError,
			Message: libs.PointerString("Не удалось прочитать запрос"),
		})
	}

	return c.JSON(http.StatusOK, structs.Result{
		Result: true,
		Code:   http.StatusOK,
		Data:   libs.PointerString(libs.Config.Classifier.Classify(text.Text, "unknow")),
	})
}

func fisher(c echo.Context) error {
	var err error
	var text struct {
		Text string `json:"text"`
	}
	err = json.NewDecoder(c.Request().Body).Decode(&text)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, structs.Result{
			Result:  false,
			Code:    http.StatusInternalServerError,
			Message: libs.PointerString("Не удалось прочитать запрос"),
		})
	}

	return c.JSON(http.StatusOK, structs.Result{
		Result: true,
		Code:   http.StatusOK,
		Data:   libs.PointerString(libs.Config.Classifier.FisherClassify(text.Text, "unknow")),
	})
}
