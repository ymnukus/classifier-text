package routes

import (
	"classify/routes/api"

	"github.com/labstack/echo/v4"
)

func Route(e *echo.Echo) {

	api.Init(e.Group("/api"))
}
