package main

import (
	"classify/libs"
	"classify/middlewares"
	"classify/routes"
	"classify/structs"
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/labstack/echo/v4"

	_ "net/http/pprof"
)

var e *echo.Echo

func init() {
	libs.LoadConfig()
	if !libs.Config.IsTrain {
		e = echo.New()
	}
}

func main() {

	newpath := filepath.Join(".", "database")
	err := os.MkdirAll(newpath, os.ModePerm)
	if err != nil {
		panic(err)
	}

	// Тренировка
	if libs.Config.IsTrain {
		train(libs.Config.Classifier)
		fo, err := os.Create(fmt.Sprintf("./database/%s.json", libs.Config.Lang))
		if err != nil {
			panic(err)
		}
		defer func() {
			if err := fo.Close(); err != nil {
				panic(err)
			}
		}()

		var j string
		j, err = libs.Config.Classifier.Save()
		if err != nil {
			panic(err)
		}
		fo.WriteString(j)

		return
	} else {
		middlewares.Init(e)
		routes.Route(e)
		e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", libs.Config.Port)))
	}
}

// Тренировка данных
func train(classifier *structs.Classifier) {
	// TODO
	f, err := os.Open(libs.Config.TraintSetPath)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)

	for {
		row, err := csvReader.Read()
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return
		}
		classifier.Train(row[0], row[1])
	}
}
