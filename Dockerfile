FROM golang:1.17.3-bullseye AS builder
WORKDIR /app/
COPY ./ ./
RUN go get
RUN CGO_ENABLED=1 GOOS=linux go build -ldflags="-extldflags=-static" -a -installsuffix cgo -o ./main ./main.go
RUN ls -l
#CMD ["/app/main"]

#FROM golang:1.17.3-alpine3.14
FROM scratch
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /
ENV TZ=Europe/Moscow
ENV ZONEINFO=/zoneinfo.zip
#RUN apk add tzdata --no-cache \
#    && cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime \
#    && echo "Europe/Moscow" >  /etc/timezone \
#    && apk del tzdata
#    && mkdir -p /database
WORKDIR /
COPY --from=builder /app/database /database/
COPY --from=builder /app/main /main
#RUN chmod +x /main
#RUN ls -l
CMD ["/main"]