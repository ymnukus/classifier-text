package libs

import (
	"classify/structs"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

var Config structs.Config

func LoadConfig() {
	var exists bool
	Config.Lang = "ru"

	var tmp string

	if tmp, exists = os.LookupEnv("PORT"); exists {
		var err error
		Config.Port, err = strconv.Atoi(tmp)
		if err != nil {
			panic(err)
		}
	} else {
		Config.Port = 3000
	}

	prepareArguments(os.Args[1:])

	Config.Classifier = structs.NewClassifier(func(str string) (result []string, err error) {
		return GetWords(str, Config.Lang)
	})

	if _, err := os.Stat(fmt.Sprintf("./database/%s.json", Config.Lang)); !errors.Is(err, os.ErrNotExist) {
		fi, err := os.Open(fmt.Sprintf("./database/%s.json", Config.Lang))
		if err != nil {
			log.Fatal(err)
		}
		defer func() {
			if err = fi.Close(); err != nil {
				log.Fatal(err)
			}
		}()

		b, err := ioutil.ReadAll(fi)
		if err != nil {
			panic(err)
		}
		err = Config.Classifier.Load(string(b))
		if err != nil {
			panic(err)
		}
	}
}

func prepareArguments(args []string) {
	if len(args) > 0 {
		switch args[0] {
		case "train":
			Config.IsTrain = true
			Config.TraintSetPath = args[1]
			prepareArguments(args[2:])
		case "lang":
			Config.Lang = args[1]
			prepareArguments(args[2:])
		case "--naivebayes":
			Config.IsNaivebayes = true
			prepareArguments(args[1:])
		case "--fisher":
			Config.IsFisher = true
			prepareArguments(args[1:])
		}
	}
}
