package libs

import (
	"regexp"
	"strings"

	"github.com/tebeka/snowball"
)

/// Подготовка текста и разбивка на слова
func GetWords(str string, lang string) (result []string, err error) {
	// Подготовим полученный текст
	var stemmer *snowball.Stemmer
	stemmer, err = snowball.New(lang)
	if err != nil {
		return
	}
	var reSplitWords *regexp.Regexp
	switch lang {
	case "ru":
		reSplitWords = regexp.MustCompile(`[^А-Яа-я]+`)
	case "en":
		reSplitWords = regexp.MustCompile(`[^A-Za-z]+`)
	}

	split := reSplitWords.Split(strings.ToLower(str), -1)

	//splitted := make(map[string]*structs.Word)
	splitted := make(map[string]*string)

	for _, word := range split {
		word = stemmer.Stem(word)
		if len([]rune(word)) > 2 && len([]rune(word)) < 20 {
			if _, ok := splitted[word]; !ok {
				// Нет слова в списке. Добавим
				splitted[word] = nil //&structs.Word{}
			}
		}
	}

	result = make([]string, 0, len(splitted))

	for k := range splitted {
		result = append(result, k)
	}

	return
}
