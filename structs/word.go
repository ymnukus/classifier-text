package structs

type Word struct {
	Categories map[string]int `json:"categories"`
}

func (word *Word) IncCat(cat string) {
	if word.Categories == nil {
		word.Categories = make(map[string]int)
	}
	(*word).Categories[cat]++
}
