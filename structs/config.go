package structs

type Config struct {
	IsTrain       bool
	TraintSetPath string

	Lang string

	IsNaivebayes bool
	IsFisher     bool

	Port int

	Classifier *Classifier
}
