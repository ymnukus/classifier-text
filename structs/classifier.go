package structs

import (
	"encoding/json"
	"log"
	"math"
)

type Classifier struct {
	Words       map[string]*Word   `json:"words"`
	Cats        map[string]int     `json:"cats"`
	Thresholds  map[string]float32 `json:"thresholds"`
	Minimums    map[string]float32 `json:"minimums"`
	getFeatures func(str string) (result []string, err error)
}

func NewClassifier(f func(str string) (result []string, err error)) *Classifier {
	classifier := Classifier{}
	classifier.initclassifier(f)
	return &classifier
}

func (classifier *Classifier) initclassifier(f func(str string) (result []string, err error)) {
	classifier.getFeatures = f
	classifier.Cats = make(map[string]int)
	classifier.Words = make(map[string]*Word)
	classifier.Thresholds = make(map[string]float32)
	classifier.Minimums = make(map[string]float32)
}

func (classifier *Classifier) SetThreshold(cat string, t float32) {
	classifier.Thresholds[cat] = t
}

func (classifier *Classifier) GetThreshold(cat string) float32 {
	if _, ok := classifier.Thresholds[cat]; !ok {
		return 1.0
	}
	return classifier.Thresholds[cat]
}

// Тренировка
func (classifier *Classifier) Train(str string, cat string) {
	if len(cat) > 0 {
		words, err := classifier.getFeatures(str)
		if err != nil {
			log.Println(err)
			return
		}
		if len(words) > 0 {
			for _, word := range words {
				classifier.incf(word, cat)
			}
			classifier.incc(cat)
		}
	}
}

// Сколько образцов отнесено к данной категории
func (classifier *Classifier) catcount(cat string) int {
	if _, ok := classifier.Cats[cat]; !ok {
		return 0
	}
	return classifier.Cats[cat]
}

// Список всех категорий
func (classifier *Classifier) Categories() []string {
	res := make([]string, 0, len(classifier.Cats))
	for k := range classifier.Cats {
		res = append(res, k)
	}
	return res
}

// Увеличить счетчик применений категории
func (classifier *Classifier) incc(cat string) {
	if _, ok := classifier.Cats[cat]; ok {
		classifier.Cats[cat]++
	} else {
		classifier.Cats[cat] = 1
	}
}

// Увеличить счетчик пар признак/категория
func (classifier *Classifier) incf(word string, cat string) {
	if _, ok := classifier.Words[word]; !ok {
		classifier.Words[word] = &Word{}
	}
	classifier.Words[word].IncCat(cat)
}

// Сколько раз признак появлялся в данной категории
func (classifier *Classifier) fcount(word string, cat string) int {
	if _, ok := classifier.Words[word]; ok {
		if _, ok := classifier.Words[word].Categories[cat]; ok {
			return classifier.Words[word].Categories[cat]
		}
	}
	return 0
}

// Условная вероятность
func (classifier *Classifier) fprob(word string, cat string) float32 {
	//if len(docclass.cats) == 0 {
	if classifier.catcount(cat) == 0 {
		return 0.0

	}
	tmp := float32(classifier.fcount(word, cat)) / float32(classifier.catcount(cat))
	return tmp
}

func (classifier *Classifier) Sampletrain() {
	classifier.Train("Клара у Карла украла караллы.", "good")
	classifier.Train("Шла Саша по шоссе и сосала сушку.", "good")
	classifier.Train("Я вычислю тебя по IP!", "bad")
	classifier.Train("Вычислю тебя", "bad")
}

type WeighteProbeStruct struct {
	Word   string
	Cat    string
	Prf    func(word string, cat string) float32
	Weight float32
	Ap     float32
}

// Взвешенная вероятность
func (classifier *Classifier) weighteprobe(weighteProbeArgs WeighteProbeStruct) float32 {
	if weighteProbeArgs.Weight == 0.0 {
		weighteProbeArgs.Weight = 1.0
	}
	if weighteProbeArgs.Ap == 0 {
		weighteProbeArgs.Ap = 0.5
	}
	basicprob := weighteProbeArgs.Prf(weighteProbeArgs.Word, weighteProbeArgs.Cat)
	totals := 0
	for _, categ := range classifier.Categories() {
		totals += classifier.fcount(weighteProbeArgs.Word, categ)
	}
	bp := ((weighteProbeArgs.Weight * weighteProbeArgs.Ap) + (float32(totals) * basicprob)) / (weighteProbeArgs.Weight + float32(totals))
	return bp
}

func (classifier *Classifier) SetMininun(cat string, val float32) {
	classifier.Minimums[cat] = val
}

func (classifier *Classifier) GetMininun(cat string) float32 {
	val, ok := classifier.Minimums[cat]
	if ok {
		return val
	}
	return 0.0
}

func (classifier *Classifier) Save() (res string, err error) {
	var bytes []byte
	bytes, err = json.Marshal(classifier)
	if err != nil {
		return
	}
	res = string(bytes)
	return
}

func (classifier *Classifier) Load(data string) (err error) {
	err = json.Unmarshal([]byte(data), classifier)
	if err != nil {
		return
	}
	return
}

// Вероятность того, что образец с указанным признаком принадлежит указанной категории, в предположении, что в каждой категории будет одинаковое число образцов
func (fisher *Classifier) cprob(word string, cat string) float32 {
	clf := fisher.fprob(word, cat)
	if clf == 0 {
		return 0.0
	}
	var freqsum float32
	for _, c := range fisher.Categories() {
		freqsum += fisher.fprob(word, c)
	}
	return clf / freqsum
}

func (fisher *Classifier) fisherprobe(str string, cat string) float32 {
	p := float32(1.0)
	words, err := fisher.getFeatures(str)
	if err != nil {
		log.Println(err)
		return 0.0
	}
	for _, word := range words {
		p *= fisher.weighteprobe(WeighteProbeStruct{
			Word: word,
			Cat:  cat,
			Prf:  fisher.cprob,
		})
	}
	fscore := float32(-2.0 * math.Log(float64(p)))
	return fisher.invchi2(fscore, len(words)*2)
}

func (fisher *Classifier) invchi2(chi float32, df int) float32 {
	m := chi / 2.0
	sum := float32(math.Exp(float64(-1.0 * m)))
	term := sum
	for i := 1; i <= int(df/2); i++ {
		term *= m / float32(i)
		sum += term
	}
	if sum < 1.0 {
		return sum
	}
	return 1.0
}

func (fisher *Classifier) FisherClassify(str string, def string) string {
	best := def
	var max float32
	for _, cat := range fisher.Categories() {
		p := fisher.fisherprobe(str, cat)
		if p > fisher.GetMininun(cat) && p > max {
			best = cat
			max = p
		}
	}
	return best
}

func (naivebayes *Classifier) docprobe(str string, cat string) float32 {
	p := float32(1.0)
	words, err := naivebayes.getFeatures(str)
	if err != nil {
		log.Println(err)
		return 0
	}
	for _, word := range words {
		p *= naivebayes.weighteprobe(WeighteProbeStruct{
			Word: word,
			Cat:  cat,
			Prf:  naivebayes.fprob,
		})
	}
	return p
}

func (naivebayes *Classifier) Probe(str string, cat string) float32 {
	catprob := naivebayes.catcount(cat)
	docprobe := naivebayes.docprobe(str, cat)
	return docprobe * float32(catprob)
}

func (naivebayes *Classifier) Classify(str string, def string) string {
	probs := make(map[string]float32)
	max := float32(0.0)
	var best string
	for _, cat := range naivebayes.Categories() {
		probs[cat] = naivebayes.Probe(str, cat)
		if probs[cat] > max {
			max = probs[cat]
			best = cat
		}
	}

	for cat := range probs {
		if cat == best {
			continue
		}
		if probs[cat]*naivebayes.GetThreshold(best) > probs[best] {
			return def
		}
	}
	return best
}
